# README #

### What is this repository for? ###

* This is the repo for the porenetwork generator build by Lucas van Oosterhout and Chaozhong Qin at the University of Utrecht.
* Version 0.2

### How do I get set up? ###

* The generator is setup by running make in the networkgen directory
* Add the moment there are no configuration steps needed to build
* A valid C++11 compiler is needed. The software builds with gcc-4.6 and clang-6.0 and higher, use other compilers at your own risk.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact